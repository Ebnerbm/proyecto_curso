/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.IDao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductOrderHandler {

    public static ArrayList<Integer> auxLstOrder = new ArrayList() {{add(1);}};
    public static ArrayList<ProductOrder> productListByOrder= new ArrayList();
    ProductOrder productOrder;

    public static void createOrder() {
        auxLstOrder.add((auxLstOrder.get(auxLstOrder.size() - 1)) + 1);
    }

    public static void resetOrder() {
        auxLstOrder.clear();
        //USO DE MAPS
        Map<Integer, ProductOrder> mapProduct = new HashMap<>(ProductOrderIdao.productList.size());
        
        for (ProductOrder p : ProductOrderIdao.productList) {
            mapProduct.put(p.getOrderID(), p);
        }
        //Agrego cada elemento del map a una nueva lista y muestro cada elemento.
        for (Map.Entry<Integer, ProductOrder> p : mapProduct.entrySet()) {
            auxLstOrder.add(p.getValue().getOrderID());
            System.out.println(p.getValue().getOrderID());
        }
        //ArrayList<Product> auxListProduct=(ArrayList<Product>) productList.stream().distinct().collect(Collectors.toList()); 
    }
    
    public void getProductListAccordingToOrder(int ordenSeleccionada){
        ProductOrder obj;
        for (int i = 0; i < ProductOrderIdao.productList.size(); i++) {
        if(ProductOrderIdao.productList.get(i).getOrderID()==ordenSeleccionada){
            obj= new ProductOrder();
            obj.setOrderID(ProductOrderIdao.productList.get(i).getOrderID());
            obj.setProductId(ProductOrderIdao.productList.get(i).getProductId());
            obj.setProductName(ProductOrderIdao.productList.get(i).getProductName());
            obj.setPrice(ProductOrderIdao.productList.get(i).getPrice());
            obj.setCantidad(ProductOrderIdao.productList.get(i).getCantidad());
            obj.setTotal(ProductOrderIdao.productList.get(i).getTotal());
            productListByOrder.add(obj);
        }
        }
    }
    
    public static void clearListByOrder(){
        productListByOrder.clear();
    }
    
    
    
   
    
}
