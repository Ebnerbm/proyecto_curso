/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.swing.table.AbstractTableModel;


public class ProductTableModelList extends AbstractTableModel implements Serializable {

   
    private List<Object[]> productsData;

    private String columnNames[];

    private static int productId = 1;

    public ProductTableModelList() {
        loadColumnNames();
        productsData = new ArrayList<>();
    }

   
    public ProductTableModelList(ProductOrder... products) {
        loadColumnNames();
        createModelFromArray(products);
    }
    
  

    public ProductTableModelList(List<Object[]> products) {
        loadColumnNames();
        createModelFromList(products);
    }

    public void setDataModel(ProductOrder... products) {
        createModelFromArray(products);
    }
    
    
    public void setDataModel(List<Object[]> products) {
        createModelFromList(products);
    }
    

    
    public List<ProductOrder> getDataModel() {
        List<ProductOrder> products = new ArrayList<>(productsData.size());
        productsData.forEach((row) -> {
            products.add(convertToProduct(row));
        });
        return products;
    }

    
    @Override
    public int getRowCount() {
        return productsData.size();
    }

    
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }
    
     public boolean isCellEditable() {
        return false;
    }

    
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return getValueAt(0, columnIndex).getClass();
    }

    
    @Override
    public String getColumnName(int column) {
        if (column < 0 || column > columnNames.length) {
            return null;
        }
        return columnNames[column];
    }

    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (productsData.isEmpty()) {
            return null;
        } else if (rowIndex < 0 || rowIndex > productsData.size()) {
            return null;
        } else if (columnIndex < 0 || columnIndex > columnNames.length) {
            return null;
        }
        return productsData.get(rowIndex)[columnIndex];
    }

    
    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (productsData.isEmpty()) {
            return;
        } else if (rowIndex < 0 || rowIndex > productsData.size()) {
            return;
        } else if (columnIndex < 0 || columnIndex > columnNames.length) {
            return;
        }
        productsData.get(rowIndex)[columnIndex] = value;
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    
    private Object[] getDefaultProduct() {
        ProductOrder product = new ProductOrder();
        product.setOrderID(1);
        product.setProductId(productId);
        product.setProductName("Nombre " + productId);
        product.setPrice(0.0D);
        product.setCantidad(0);
        product.setTotal(0);
        
        productId++;
        return new Object[]{ product.getOrderID(),product.getProductId(), product.getProductName(),
            product.getPrice(), product.getCantidad(),product.getTotal()};
    }

    
    public void addRow() {
        addRow(getDefaultProduct());
    }

    
    public void addRow(Object[] product) {
        if (product != null && product.length > 0) {
            productsData.add(product);
            fireTableRowsInserted(productsData.size() - 1, productsData.size() - 1);
        }
    }

    
    public void removeRow(int selectedRow) {
        if (selectedRow >= 0 && selectedRow < productsData.size()) {
            productsData.remove(selectedRow);
            fireTableRowsDeleted(selectedRow, selectedRow);
        }
    }

    private void loadColumnNames() {
        Field fields[] = ProductOrder.class.getDeclaredFields();
        columnNames = new String[fields.length];
        int index = 0;
        for (Field field : fields) {
            columnNames[index++] = field.getName();
        }
        fireTableStructureChanged();
    }

    public String[] setColumnNames(){
        Field fields[] = ProductOrder.class.getDeclaredFields();
        columnNames = new String[fields.length];
        int index = 0;
        for (Field field : fields) {
            columnNames[index++] = field.getName();
        }
        fireTableStructureChanged();
        return columnNames;
    }
    
    private void createModelFromArray(ProductOrder[] productsArray) {
        if (productsArray != null && productsArray.length > 0) {
            productsData = new ArrayList<>(productsArray.length);
            Stream.of(productsArray).forEach((p) -> {
                productsData.add(convertToArray(p));
            });
            fireTableDataChanged();
        } else {
            productsData = new ArrayList<>();
            fireTableDataChanged();
        }
    }

    private Object[] convertToArray(ProductOrder p) {
        Field fields[] = ProductOrder.class.getDeclaredFields();
        Object[] row = new Object[fields.length];
        int index = 0;
        for (Field f : fields) {
            f.setAccessible(true);
            try {
                row[index++] = f.get(p);
            } catch (IllegalArgumentException | IllegalAccessException ex) {
                Logger.getLogger(ProductTableModelList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return row;
    }

    private void createModelFromList(List<Object[]> productsList) {
        if (productsList != null && !productsList.isEmpty()) {
            productsData = new ArrayList<>(productsList);
            fireTableDataChanged();
        } else {
            productsData = new ArrayList<>();
            fireTableDataChanged();
        }
    }

    private ProductOrder convertToProduct(Object[] row) {
        ProductOrder temp = new ProductOrder();
     //   temp.setPersonName(String.valueOf(row[0].toString()));
        temp.setOrderID(Integer.parseInt(row[0].toString()));
        temp.setProductId(Integer.parseInt(row[1].toString()));
        temp.setProductName(String.valueOf(row[2]));
        temp.setPrice(Double.parseDouble(row[3].toString()));
        temp.setCantidad(Integer.parseInt(row[4].toString()));
        temp.setTotal(Double.parseDouble(row[5].toString()));
        

        return temp;
    }

}
