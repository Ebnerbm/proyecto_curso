/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProductOrder implements Serializable {

    private int orderID;
    private int productId;
    private String productName;
    private double price;
    private int quantity;
    private double total;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int serialNumber) {
        this.orderID = serialNumber;
    }

    public void setTotal(int cantidad, double price) {
        total = cantidad * price;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public int getCantidad() {
        return quantity;
    }

    public void setCantidad(int cantidad) {
        this.quantity = cantidad;
    }

    public ProductOrder() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Producto{"+"Numero de orden= "+orderID + ", Id del Producto=" + productId + ", Nombre del platillo =" + productName + ", Precio=" + price + ", Cantidad=" + quantity
                + ", Total=" + total + '}';
    }
    
    
    
      public static double getTotalCompras(ArrayList<ProductOrder> productList){
        double total=0;
        for (int i = 0; i < productList.size(); i++) {
          total=total+productList.get(i).getTotal();
        }
        return total;
    }
    
     public static int getTotalCantidad(ArrayList<ProductOrder> productList){
        int total=0;
        for (int i = 0; i < productList.size(); i++) {
          total=total+productList.get(i).getCantidad();
        }
        return total;
    }
}
