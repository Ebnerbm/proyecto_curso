/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.IDao;
import java.util.ArrayList;
import model.ProductOrder;



public class ProductOrderIdao implements IDao<ProductOrder> {
    ProductOrder product;
    public static ArrayList<ProductOrder> productList= new ArrayList<>();
   
    @Override
    public void create(ProductOrder object) {
       int n=productList.size();
       //object.setOrderID();
       object.setProductId(n+1);
       object.getProductName();
       object.getPrice();
       object.getCantidad();
       object.setTotal(object.getCantidad(),object.getPrice());
       
       productList.add(object);    
    }
    
    
     @Override
    public boolean update(ProductOrder object) {
        boolean flag=false;
        for (int i = 0; i < productList.size(); i++) {
            if(object.getProductId()==(productList.get(i).getProductId())){
            productList.get(i).setCantidad(object.getCantidad());
            productList.get(i).setTotal(object.getCantidad(),object.getPrice());
            productList.get(i).setOrderID(object.getOrderID());
            flag=true;
            break;
            }
          }
        return flag;
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < productList.size(); i++) {
            if(id==(productList.get(i).getProductId())){
                productList.remove(i);
            }
        }
    }

    @Override
    public ArrayList<ProductOrder> finAll() {
        return productList;
    }
    
       @Override
    public ProductOrder findById(int id) {
        product= new ProductOrder();
        for (int i = 0; i < productList.size(); i++) {
            if(id==(productList.get(i).getProductId())){
                product.setProductId(id);
                product.setProductName(productList.get(i).getProductName());
                product.setPrice(productList.get(i).getPrice());
                product.setCantidad(productList.get(i).getCantidad());
                product.setTotal(productList.get(i).getTotal());
                break;
            }
        }
        return product;
    }

    public void updateList(ArrayList<ProductOrder> finAll) {
        productList.clear();
        productList=finAll;
    }
    
    public void clearList(){
        productList.clear();
    }
    
    /*public static double getTotalCompras(ArrayList<ProductOrder> productList){
        double total=0;
        for (int i = 0; i < productList.size(); i++) {
          total=total+productList.get(i).getTotal();
        }
        return total;
    }
    
     public static int getTotalCantidad(ArrayList<ProductOrder> productList){
        int total=0;
        for (int i = 0; i < productList.size(); i++) {
          total=total+productList.get(i).getCantidad();
        }
        return total;
    }*/
    
}
