/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;


/**
 *
 * @author willj
 */
public class Product implements Serializable {
    
    private int productId;
    private String productName;
    private double price;
    private int quantity;
    private double total;
    
    
    public void setTotal(int cantidad, double price){
        total= cantidad*price;
    }
    
    public void setTotal(double total){
        this.total=total;
    }
     
    public double getTotal(){
        return total;
    }


    public int getCantidad() {
        return quantity;
    }

    public void setCantidad(int cantidad) {
        this.quantity = cantidad;
    }
    
    public Product() {
    }


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Producto{"+"Id=" + productId + ", Nombre del platillo =" + productName +  ", Precio=" + price + ", Cantidad=" + quantity+ 
                ", Total=" + total  + '}';
    }
}
