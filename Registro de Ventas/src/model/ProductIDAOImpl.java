/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import interfaces.IDao;
import java.awt.List;
import java.util.ArrayList;
import model.Product;



public class ProductIDAOImpl implements IDao<Product> {
    Product product;
    public static ArrayList<Product> list= new ArrayList<>();
    int n =0;

   

    @Override
    public void create(Product object) {
       n=list.size();
       object.setProductId(n+1);
       object.getProductName();
       object.getPrice();
       object.getCantidad();
       object.setTotal(object.getCantidad(),object.getPrice());
       
       list.add(object);    
    }
    
    
     @Override
    public boolean update(Product object) {
        boolean flag=false;
        for (int i = 0; i < list.size(); i++) {
            if(object.getProductId()==(list.get(i).getProductId())){
            list.get(i).setCantidad(object.getCantidad());
            list.get(i).setTotal(object.getCantidad(),object.getPrice());
            flag=true;
            break;
            }
          }
        return flag;
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < list.size(); i++) {
            if(id==(list.get(i).getProductId())){
                list.remove(i);
            }
        }
    }

    @Override
    public ArrayList<Product> finAll() {
        return list;
    }
    
       @Override
    public Product findById(int id) {
        product= new Product();
        for (int i = 0; i < list.size(); i++) {
            if(id==(list.get(i).getProductId())){
                product.setProductId(id);
                product.setProductName(list.get(i).getProductName());
                product.setPrice(list.get(i).getPrice());
                product.setCantidad(list.get(i).getCantidad());
                product.setTotal(list.get(i).getTotal());
                break;
            }
        }
        return product;
    }

    public void updateList(ArrayList<Product> finAll) {
        list.clear();
        list=finAll;
    }
    
    public void clearList(){
        list.clear();
    }
    
    public static double getTotalCompras(){
        double total=0;
        for (int i = 0; i < list.size(); i++) {
          total=total+list.get(i).getTotal();
        }
        return total;
    }
    
     public static int getTotalCantidad(){
        int total=0;
        for (int i = 0; i < list.size(); i++) {
          total=total+list.get(i).getCantidad();
        }
        return total;
    }
    
}
