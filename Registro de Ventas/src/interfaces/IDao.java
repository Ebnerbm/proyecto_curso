/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;


public interface IDao<T> {
    
    ArrayList<T> finAll();
    
    T findById(final int id);

    // Updating methods =====
    boolean update(final T persistentObject);
    
    void create (final T object);


    void delete(final int id);
    

}
