/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.ProductController;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.table.TableModel;
import model.ProductOrderHandler;
import model.ProductOrder;
import model.ProductOrderIdao;
import model.ProductTableModelList;

public class PnlVentas extends javax.swing.JPanel {

    private ProductController controller;
    private ProductTableModelList tableModelList;
    private ProductOrderIdao daoProduct;
    
    public PnlVentas() {
        initComponents();
        setUpProductTableList();
        setUpController();
        setTotalQuantityAndPurchases();
    }
    
   
    private void setUpProductTableList() {
        tableModelList = new ProductTableModelList();
        productListTable.setModel(tableModelList);
    }
    
    
    public void showListOnTable1(){ 
         setUpProductTableList();
         for (int i =0; i < ProductOrderIdao.productList.size(); i++) {
             tableModelList.addRow(new Object[] {ProductOrderIdao.productList.get(i).getOrderID(),ProductOrderIdao.productList.get(i).getProductId(),
                 ProductOrderIdao.productList.get(i).getProductName(),
                 ProductOrderIdao.productList.get(i).getPrice(),
                 ProductOrderIdao.productList.get(i).getCantidad(), 
                 ProductOrderIdao.productList.get(i).getTotal()});           
        }
        productListTable.setModel(tableModelList);
        
    }
    
    public void setTotalQuantityAndPurchases(){
        lblUnidades.setText(String.valueOf(ProductOrder.getTotalCantidad(ProductOrderIdao.productList)));
        lblTotalCompras.setText("C$ "+String.valueOf(ProductOrder.getTotalCompras(ProductOrderIdao.productList)));
    }

    public ProductOrder[] getDataTable() {
        ProductOrder[] productList;
        int rowCount = this.productListTable.getModel().getRowCount();
        if (rowCount > 0) {
            productList = new ProductOrder[rowCount];
            for (int i = 0; i < rowCount; i++) {
                ProductOrder p = new ProductOrder();
                p.setOrderID(Integer.parseInt(productListTable.getValueAt(i, 0).toString()));
                p.setProductId(Integer.parseInt(productListTable.getValueAt(i, 1).toString()));
                p.setProductName(String.valueOf(productListTable.getModel().getValueAt(i, 2)));
                p.setPrice(Double.parseDouble(productListTable.getModel().getValueAt(i, 3).toString()));
                p.setCantidad(Integer.parseInt(productListTable.getValueAt(i, 4).toString()));
                p.setTotal(Double.parseDouble(productListTable.getModel().getValueAt(i, 5).toString()));
                
                productList[i] = p;
            }
        } else {
            productList = new ProductOrder[0];
        }
        return productList;
    }

    public void setProductTableModel(TableModel model) {
        productListTable.setModel(model);
        ProductOrderIdao daoProduct= new ProductOrderIdao();
        daoProduct.clearList();
        ProductOrderIdao.productList= new ArrayList<>(Arrays.asList(getDataTable()));
    }
    
    /**
     *
     */
    public void addRow() {
//        tableModel = (ProductTableModel) productListTable.getModel();
//        tableModel.addRow(ProductTableModel.getDefaultRowData());
        tableModelList = (ProductTableModelList) productListTable.getModel();
        tableModelList.addRow();
        tableModelList.isCellEditable();
    }
    
    


    /**
     *
     */
    public void removeRow() {
//        tableModel = (ProductTableModel) productListTable.getModel();
//        tableModel.removeRow(productListTable.getSelectedRow());
        daoProduct= new ProductOrderIdao();
        tableModelList =  (ProductTableModelList) productListTable.getModel();
        int selected=productListTable.getSelectedRow();
        int id=ProductOrderIdao.productList.get(selected).getProductId();
        daoProduct.delete(id);
        tableModelList.removeRow(productListTable.getSelectedRow());
       
        
    }
     private void setUpController() {
        controller = new ProductController(this);
        this.deleteRowButton.addActionListener(controller);
        this.saveButton.addActionListener(controller);
        this.openButton.addActionListener(controller);
        this.showButton.addActionListener(controller);
        this.updateButton.addActionListener(controller);
        this.invoiceButton.addActionListener(controller);
        
    }
     
     
    public ProductOrder returnProduct() {
        daoProduct= new ProductOrderIdao();
        tableModelList =  (ProductTableModelList) productListTable.getModel();
        int selected=productListTable.getSelectedRow();
        int id=ProductOrderIdao.productList.get(selected).getProductId();
        return daoProduct.findById(id);          
    }
    
    
    public void resetOrders() {
      ProductOrderHandler.resetOrder();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnlBotones = new javax.swing.JPanel();
        invoiceButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        saveButton = new javax.swing.JButton();
        deleteRowButton = new javax.swing.JButton();
        openButton = new javax.swing.JButton();
        showButton = new javax.swing.JButton();
        updateButton = new javax.swing.JButton();
        PnlTabla = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productListTable = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblUnidades = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblTotalCompras = new javax.swing.JLabel();

        setBackground(new java.awt.Color(204, 204, 204));
        setPreferredSize(new java.awt.Dimension(1200, 800));
        setLayout(new java.awt.BorderLayout());

        pnlBotones.setBackground(new java.awt.Color(255, 82, 27));
        pnlBotones.setPreferredSize(new java.awt.Dimension(1014, 75));
        pnlBotones.setLayout(new java.awt.GridBagLayout());

        invoiceButton.setText("Facturar orden");
        invoiceButton.setActionCommand("invoice");
        invoiceButton.setPreferredSize(new java.awt.Dimension(105, 30));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        pnlBotones.add(invoiceButton, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("REGISTRO DE VENTAS");
        jLabel2.setPreferredSize(new java.awt.Dimension(500, 40));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 0.1;
        pnlBotones.add(jLabel2, gridBagConstraints);

        jPanel2.setPreferredSize(new java.awt.Dimension(800, 35));
        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        saveButton.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        saveButton.setText("Guardar");
        saveButton.setActionCommand("saveTableContent");
        saveButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        saveButton.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        saveButton.setPreferredSize(new java.awt.Dimension(130, 30));
        jPanel2.add(saveButton);

        deleteRowButton.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        deleteRowButton.setText("Eliminar Compras");
        deleteRowButton.setActionCommand("deleteRow");
        deleteRowButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        deleteRowButton.setPreferredSize(new java.awt.Dimension(130, 30));
        jPanel2.add(deleteRowButton);

        openButton.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        openButton.setText("Abrir");
        openButton.setActionCommand("open");
        openButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        openButton.setPreferredSize(new java.awt.Dimension(130, 30));
        jPanel2.add(openButton);

        showButton.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        showButton.setText("Mostrar Compras");
        showButton.setActionCommand("show");
        showButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        showButton.setPreferredSize(new java.awt.Dimension(130, 30));
        jPanel2.add(showButton);

        updateButton.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        updateButton.setText("Actualizar");
        updateButton.setActionCommand("update");
        updateButton.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        updateButton.setPreferredSize(new java.awt.Dimension(130, 30));
        jPanel2.add(updateButton);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        pnlBotones.add(jPanel2, gridBagConstraints);

        add(pnlBotones, java.awt.BorderLayout.NORTH);

        PnlTabla.setBackground(new java.awt.Color(0, 204, 255));
        PnlTabla.setLayout(new java.awt.BorderLayout());

        productListTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        productListTable.setToolTipText("");
        jScrollPane1.setViewportView(productListTable);

        PnlTabla.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setPreferredSize(new java.awt.Dimension(300, 219));

        jLabel1.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        jLabel1.setText("Unidades Compradas:");

        lblUnidades.setFont(new java.awt.Font("Yu Gothic UI Light", 0, 14)); // NOI18N
        lblUnidades.setText("Unidades");
        lblUnidades.setToolTipText("");

        jLabel3.setFont(new java.awt.Font("Calibri Light", 1, 18)); // NOI18N
        jLabel3.setText("Total de compras:");

        lblTotalCompras.setFont(new java.awt.Font("Yu Gothic UI Light", 0, 14)); // NOI18N
        lblTotalCompras.setText("C$");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 265, Short.MAX_VALUE)
                    .addComponent(lblUnidades, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblTotalCompras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(lblUnidades)
                .addGap(42, 42, 42)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(lblTotalCompras)
                .addContainerGap(517, Short.MAX_VALUE))
        );

        PnlTabla.add(jPanel1, java.awt.BorderLayout.EAST);

        add(PnlTabla, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel PnlTabla;
    private javax.swing.JButton deleteRowButton;
    private javax.swing.JButton invoiceButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblTotalCompras;
    private javax.swing.JLabel lblUnidades;
    private javax.swing.JButton openButton;
    private javax.swing.JPanel pnlBotones;
    private javax.swing.JTable productListTable;
    private javax.swing.JButton saveButton;
    private javax.swing.JButton showButton;
    private javax.swing.JButton updateButton;
    // End of variables declaration//GEN-END:variables

 

   
}
