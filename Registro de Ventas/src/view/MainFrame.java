
package view;

import controller.MainController;
import javax.swing.JOptionPane;
import misc.Constants;
import misc.PropertyFile;


public class MainFrame extends javax.swing.JFrame {
    PnlMenu pnlMenu;
    PnlVentas PnlVentas;
    MainController mc;

    public MainFrame() {
        initComponents();
        mc = new MainController(this);
        
        pnlContainer.add(pnlMenu = new PnlMenu(), "pnlMenu");
        pnlContainer.add(PnlVentas = new PnlVentas(), "pnlVentas");
        setLocationRelativeTo(null);
        setController();
        applySettings();
    }
    
    public void setController() {
        btnMenu.addActionListener(mc);
        btnCompras.addActionListener(mc);
        this.btnConfiguration.addActionListener(mc);
    }
    
    public void mostrarPanel(String identificador) {
        ((java.awt.CardLayout)pnlContainer.getLayout()).show(pnlContainer, identificador);
    }
    
     public void applySettings(){
        PropertyFile pf = new PropertyFile();
        pf.open();
        if(pf.isFileExists()){
            labelNameRest.setText(pf.getProperty(Constants.COMPANYNAME_PROPERTY));
        }
        else{
            if(JOptionPane.showConfirmDialog(this, "Aún no se ha realizado la configuración inicial. ¿Deseas hacerla ahora?", "Configurar",
                    JOptionPane.YES_NO_OPTION , JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION){
                mc.showDialogSettings();
                pf.open();
                labelNameRest.setText(pf.getProperty(Constants.COMPANYNAME_PROPERTY));
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlControles = new javax.swing.JPanel();
        btnMenu = new javax.swing.JButton();
        btnCompras = new javax.swing.JButton();
        btnConfiguration = new javax.swing.JButton();
        pnlContainer = new javax.swing.JPanel();
        pnlConfig = new javax.swing.JPanel();
        labelNameRest = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAutoRequestFocus(false);

        pnlControles.setBackground(new java.awt.Color(51, 51, 51));
        pnlControles.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        pnlControles.setMaximumSize(new java.awt.Dimension(200, 100));
        pnlControles.setMinimumSize(new java.awt.Dimension(200, 100));
        pnlControles.setPreferredSize(new java.awt.Dimension(200, 100));

        btnMenu.setBackground(new java.awt.Color(255, 82, 27));
        btnMenu.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnMenu.setForeground(new java.awt.Color(255, 255, 255));
        btnMenu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/menu.png"))); // NOI18N
        btnMenu.setText("Menú");
        btnMenu.setActionCommand("Menu");
        btnMenu.setContentAreaFilled(false);
        btnMenu.setFocusPainted(false);
        btnMenu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnMenu.setOpaque(true);
        btnMenu.setPreferredSize(new java.awt.Dimension(150, 100));
        btnMenu.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pnlControles.add(btnMenu);

        btnCompras.setBackground(new java.awt.Color(255, 82, 27));
        btnCompras.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnCompras.setForeground(new java.awt.Color(255, 255, 255));
        btnCompras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/ventas.png"))); // NOI18N
        btnCompras.setText("Ventas");
        btnCompras.setContentAreaFilled(false);
        btnCompras.setFocusPainted(false);
        btnCompras.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCompras.setOpaque(true);
        btnCompras.setPreferredSize(new java.awt.Dimension(150, 100));
        btnCompras.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pnlControles.add(btnCompras);

        btnConfiguration.setBackground(new java.awt.Color(255, 82, 27));
        btnConfiguration.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnConfiguration.setForeground(new java.awt.Color(255, 255, 255));
        btnConfiguration.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/Configuración.png"))); // NOI18N
        btnConfiguration.setText("Configuración");
        btnConfiguration.setActionCommand("configuration");
        btnConfiguration.setContentAreaFilled(false);
        btnConfiguration.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnConfiguration.setOpaque(true);
        btnConfiguration.setPreferredSize(new java.awt.Dimension(150, 100));
        btnConfiguration.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pnlControles.add(btnConfiguration);

        getContentPane().add(pnlControles, java.awt.BorderLayout.WEST);

        pnlContainer.setBackground(new java.awt.Color(153, 153, 153));
        pnlContainer.setMaximumSize(new java.awt.Dimension(0, 0));
        pnlContainer.setPreferredSize(new java.awt.Dimension(1216, 800));
        pnlContainer.setLayout(new java.awt.CardLayout());
        getContentPane().add(pnlContainer, java.awt.BorderLayout.CENTER);

        labelNameRest.setFont(new java.awt.Font("Arial Rounded MT Bold", 1, 16)); // NOI18N
        pnlConfig.add(labelNameRest);

        getContentPane().add(pnlConfig, java.awt.BorderLayout.PAGE_START);

        setBounds(0, 0, 1416, 838);
    }// </editor-fold>//GEN-END:initComponents

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCompras;
    private javax.swing.JButton btnConfiguration;
    private javax.swing.JButton btnMenu;
    private javax.swing.JLabel labelNameRest;
    private javax.swing.JPanel pnlConfig;
    private javax.swing.JPanel pnlContainer;
    private javax.swing.JPanel pnlControles;
    // End of variables declaration//GEN-END:variables
}
