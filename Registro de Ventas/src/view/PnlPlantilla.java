/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.PlantillaController;
import model.Platillo;

public class PnlPlantilla extends javax.swing.JPanel {
    Platillo plato;
    
    
    private PlantillaController plantillaController;

    public PnlPlantilla(Platillo plato) {
        initComponents();
        
        this.plato = plato;
        
        setDatos();
        setUpController();
        
    }
    
    public void setDatos() {
        try {
            lblImage.setIcon(new javax.swing.ImageIcon(getClass().getResource(plato.getImagenPlatillo())));
        } catch (Exception e) {
            
            lblImage.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/platillos/Hamburguesa Sencilla de Res.jpg")));
        }
        lblNombre.setText(plato.getNombrePlatillo());
        lblPrecio.setText("C$ "+plato.getPrecioPlatillo());
    }
    
      private void setUpController() {
        plantillaController = new PlantillaController(plato,this);
        this.comprasButton.addActionListener(plantillaController);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        lblImage = new javax.swing.JLabel();
        lblNombre = new javax.swing.JLabel();
        lblPrecio = new javax.swing.JLabel();
        comprasButton = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204)));
        setMinimumSize(new java.awt.Dimension(300, 400));
        setPreferredSize(new java.awt.Dimension(300, 400));
        setLayout(new java.awt.GridBagLayout());

        lblImage.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(11, 0, 11, 0);
        add(lblImage, gridBagConstraints);

        lblNombre.setBackground(new java.awt.Color(255, 255, 255));
        lblNombre.setFont(new java.awt.Font("Segoe UI", 0, 20)); // NOI18N
        lblNombre.setForeground(new java.awt.Color(255, 82, 27));
        lblNombre.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblNombre.setText("Nombre del Producto");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 9, 0);
        add(lblNombre, gridBagConstraints);

        lblPrecio.setBackground(new java.awt.Color(255, 255, 255));
        lblPrecio.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblPrecio.setForeground(new java.awt.Color(255, 82, 27));
        lblPrecio.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPrecio.setLabelFor(lblPrecio);
        lblPrecio.setText("Precio del Producto");
        lblPrecio.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.insets = new java.awt.Insets(9, 0, 9, 0);
        add(lblPrecio, gridBagConstraints);

        comprasButton.setBackground(new java.awt.Color(255, 82, 27));
        comprasButton.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        comprasButton.setForeground(new java.awt.Color(255, 255, 255));
        comprasButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/Carrito.png"))); // NOI18N
        comprasButton.setActionCommand("compras");
        comprasButton.setContentAreaFilled(false);
        comprasButton.setFocusPainted(false);
        comprasButton.setOpaque(true);
        comprasButton.setPreferredSize(new java.awt.Dimension(120, 60));
        comprasButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        comprasButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comprasButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.insets = new java.awt.Insets(12, 0, 12, 0);
        add(comprasButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void comprasButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comprasButtonActionPerformed
       // Operaciones n= new Operaciones(plato);
       // n.setVisible(true);
    }//GEN-LAST:event_comprasButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton comprasButton;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel lblNombre;
    private javax.swing.JLabel lblPrecio;
    // End of variables declaration//GEN-END:variables
}
