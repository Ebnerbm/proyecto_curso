
package view;

import controller.MenuController;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.Menu;
import model.ProductOrderHandler;
import model.Platillo;

public class PnlMenu extends javax.swing.JPanel {
    ArrayList<Platillo> listaPlatillos;
    ArrayList<PnlPlantilla> plantillasPlatos;
    Menu menu;
    MenuController menuController;

    public PnlMenu() {
        initComponents();
        
        //System.out.println(pnlContenedor.getLayout().minimumLayoutSize(pnlContenedor));
        
        listaPlatillos = (menu = new Menu()).getPlatillos();
        plantillasPlatos = new ArrayList<PnlPlantilla>();
        
        addPaneles();
        setUpController();
    }
    
    public void addPaneles() {
        for (int i = 0; i < listaPlatillos.size(); i++) {
            plantillasPlatos.add(new PnlPlantilla(listaPlatillos.get(i)));
            pnlContenedor.add(plantillasPlatos.get(i));
            
        }
        pnlContenedor.setSize(pnlContenedor.getLayout().preferredLayoutSize(pnlContenedor));
        
        plantillasPlatos.trimToSize();
    }
    
    private void setUpController(){
        menuController= new MenuController(this);
        this.btnCreateOrder.addActionListener(menuController);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnCreateOrder = new javax.swing.JButton();
        scrollMenu = new javax.swing.JScrollPane();
        pnlContenedor = new javax.swing.JPanel();

        setForeground(new java.awt.Color(102, 102, 102));
        setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 82, 27));
        jPanel1.setPreferredSize(new java.awt.Dimension(10, 75));
        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setFont(new java.awt.Font("Segoe UI", 0, 20)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("MENU");
        jLabel1.setPreferredSize(new java.awt.Dimension(100, 50));
        jPanel1.add(jLabel1, new java.awt.GridBagConstraints());

        jPanel2.setOpaque(false);
        jPanel2.setPreferredSize(new java.awt.Dimension(110, 70));
        jPanel2.setLayout(new javax.swing.BoxLayout(jPanel2, javax.swing.BoxLayout.LINE_AXIS));

        btnCreateOrder.setBackground(new java.awt.Color(51, 51, 51));
        btnCreateOrder.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnCreateOrder.setForeground(new java.awt.Color(255, 255, 255));
        btnCreateOrder.setText("Crear orden");
        btnCreateOrder.setActionCommand("crearOrden");
        btnCreateOrder.setContentAreaFilled(false);
        btnCreateOrder.setFocusPainted(false);
        btnCreateOrder.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnCreateOrder.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnCreateOrder.setOpaque(true);
        btnCreateOrder.setPreferredSize(new java.awt.Dimension(130, 40));
        btnCreateOrder.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCreateOrder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateOrderActionPerformed(evt);
            }
        });
        jPanel2.add(btnCreateOrder);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.1;
        jPanel1.add(jPanel2, gridBagConstraints);

        add(jPanel1, java.awt.BorderLayout.PAGE_START);

        scrollMenu.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollMenu.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        pnlContenedor.setBackground(new java.awt.Color(201, 233, 252));
        pnlContenedor.setPreferredSize(new java.awt.Dimension(1216, 1400));
        pnlContenedor.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 50, 50));
        scrollMenu.setViewportView(pnlContenedor);

        add(scrollMenu, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateOrderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateOrderActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCreateOrderActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCreateOrder;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel pnlContenedor;
    private javax.swing.JScrollPane scrollMenu;
    // End of variables declaration//GEN-END:variables

    public void createOrder() {
        //ProductIDAOImpl daoProduct= new ProductOrderIdao();
        //daoProduct.createOrder();
        ProductOrderHandler.createOrder();
        JOptionPane.showMessageDialog(null,"Numero de orden creado: "+ ProductOrderHandler.auxLstOrder.get(ProductOrderHandler.auxLstOrder.size()-1)+"\n Tamaño:");
       
    }
}
