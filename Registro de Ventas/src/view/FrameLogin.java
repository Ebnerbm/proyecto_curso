/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.LoginController;
import java.util.Arrays;
import javax.swing.JOptionPane;
import model.User;

public class FrameLogin extends javax.swing.JFrame {
    LoginController lc;
 
    /**
     * Creates new form Ventana
     */
     public FrameLogin() {
        initComponents();
        setupController();
    }
    public User getData(){
        User user = new User();
        user.setUserName(this.txtFieldUser.getText());
        user.setPassword(Arrays.toString(this.txtFieldPassword.getPassword()));
        return user;
    }
    public void setupController(){
        lc = new LoginController(this);
        this.btnLogin.addActionListener(lc);
        this.btnExit.addActionListener(lc);
    }
    
    public void clear(){
        txtFieldUser.setText("");
        txtFieldPassword.setText("");
        
    }
   
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnExit = new javax.swing.JButton();
        imagenUsuario = new javax.swing.JLabel();
        etiquetaUsuario = new javax.swing.JLabel();
        etiquetaContraseña = new javax.swing.JLabel();
        txtFieldUser = new javax.swing.JTextField();
        btnLogin = new javax.swing.JButton();
        txtFieldPassword = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(201, 233, 252));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(204, 204, 204), 2));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnExit.setBackground(new java.awt.Color(255, 82, 27));
        btnExit.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnExit.setForeground(new java.awt.Color(255, 255, 255));
        btnExit.setText("Cancelar");
        btnExit.setActionCommand("CancelLogin");
        btnExit.setContentAreaFilled(false);
        btnExit.setFocusPainted(false);
        btnExit.setOpaque(true);
        jPanel1.add(btnExit, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 10, 110, 30));

        imagenUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resource/images/logo.png"))); // NOI18N
        jPanel1.add(imagenUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, -1, -1));

        etiquetaUsuario.setBackground(new java.awt.Color(255, 82, 27));
        etiquetaUsuario.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        etiquetaUsuario.setForeground(new java.awt.Color(255, 82, 27));
        etiquetaUsuario.setText("Usuario:");
        jPanel1.add(etiquetaUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 310, 100, 40));

        etiquetaContraseña.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        etiquetaContraseña.setForeground(new java.awt.Color(255, 82, 27));
        etiquetaContraseña.setText("Contraseña:");
        jPanel1.add(etiquetaContraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 370, -1, -1));

        txtFieldUser.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtFieldUser, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 320, 230, 30));

        btnLogin.setBackground(new java.awt.Color(255, 82, 27));
        btnLogin.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        btnLogin.setForeground(new java.awt.Color(255, 255, 255));
        btnLogin.setText("Ingresar");
        btnLogin.setActionCommand("btnLogin");
        btnLogin.setContentAreaFilled(false);
        btnLogin.setFocusPainted(false);
        btnLogin.setOpaque(true);
        jPanel1.add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 450, 120, 40));

        txtFieldPassword.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtFieldPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 370, 230, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    
    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel etiquetaContraseña;
    private javax.swing.JLabel etiquetaUsuario;
    private javax.swing.JLabel imagenUsuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField txtFieldPassword;
    private javax.swing.JTextField txtFieldUser;
    // End of variables declaration//GEN-END:variables
}
