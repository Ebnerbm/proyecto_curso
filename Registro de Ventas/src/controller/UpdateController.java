/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.OperacionEditar;


public class UpdateController implements ActionListener {

    OperacionEditar list;
    
    public UpdateController(OperacionEditar list){
        this.list=list;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "editar":
               
                list.updateAction();
                list.dispose();
            
                break;
            case "Salir":
                
                list.dispose();
                
                break;
    }
    
}
}
