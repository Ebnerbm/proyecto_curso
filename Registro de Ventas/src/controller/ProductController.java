/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.ProductOrder;
import model.ProductTableModelList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import model.ProductOrderIdao;
import view.InvoiceOrder;
import view.OperacionEditar;
import view.PnlVentas;


public class ProductController extends KeyAdapter implements ActionListener {

    private PnlVentas list;
    private JFileChooser dialog;
    OperacionEditar showOpEd;
    InvoiceOrder invoice;

    
     
    public ProductController(PnlVentas list) {
        dialog = new JFileChooser();
        this.list = list;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
           
            case "open":
                showProductsOnTable();
                list.setTotalQuantityAndPurchases();
                list.resetOrders();
                break;
            case "newRow":
                list.addRow();
                break;
            case "saveTableContent":
                saveTableContent();
                break;
            case "deleteRow":
                list.removeRow();
                list.setTotalQuantityAndPurchases();
                break;
            case "show":
                list.showListOnTable1();
                list.setTotalQuantityAndPurchases();
                break;
            case "update":
                 showOpEd= new OperacionEditar(list.returnProduct());
                 showOpEd.setVisible(true);
                break;
            case "invoice":
                invoice= new InvoiceOrder(ProductOrderIdao.productList);
                invoice.setVisible(true);
          
        }
    }

 

    private void writeFile(File file, ProductOrder product) {
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(product);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private ProductOrder readFile(File file) {
        ProductOrder product = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            product = (ProductOrder) in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return product;
    }

    private ProductOrder[] readProductList(File file) {
        ProductOrder products[] = null;
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(file))) {
            products = (ProductOrder[]) input.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return products;
    }

    /**
     *  CAMBIO DE MODELO EN LA LINEA 145
     */
    private void showProductsOnTable() {
        dialog.showOpenDialog(list);
        File file = dialog.getSelectedFile();
        if (file != null) {
            //list.setProductTableModel(new ProductTableModel(readProductList(file)));
            list.setProductTableModel(new ProductTableModelList(readProductList(file)));
        }
    }

    private void saveTableContent() {
        dialog.showSaveDialog(list);
        ProductOrder products[] = list.getDataTable();
        try (ObjectOutputStream output
                = new ObjectOutputStream(new FileOutputStream(dialog.getSelectedFile()))) {
            output.writeObject(products);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        JTextField input = (JTextField) e.getSource();
        switch (input.getName()) {
            case "price":
                char c = e.getKeyChar();
                if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE 
                        || c == KeyEvent.VK_ENTER || c == KeyEvent.VK_PERIOD)) {
                    e.consume();
                }
                break;
            case "productName":
                if (input.getText().length() >= 10) {
                    e.consume();
                }
                break;
                
            case "personName":
                if (input.getText().length() >= 10) {
                    e.consume();
                }
                break;
        }
    }
}
