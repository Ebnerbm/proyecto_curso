/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import model.ProductOrderHandler;
import view.InvoiceOrder;

/**
 *
 * @author agsch
 */
public class InvoiceController implements ActionListener,ItemListener{

    InvoiceOrder lst;
    
    public InvoiceController(InvoiceOrder lst) {
       this.lst=lst;
    }
    @Override
    public void actionPerformed(ActionEvent e) {    
        switch (e.getActionCommand()) {
            case "invoiceGenerate":
                
                break;
                
            case "salir":
                
                lst.dispose();
                
                break;
            default:
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {    
        ProductOrderHandler.clearListByOrder();
        int orderID = lst.getSelectedItemCmb();
        ProductOrderHandler productHandler= new ProductOrderHandler();
        productHandler.getProductListAccordingToOrder(orderID);
       
        lst.showListOnTable1();
        lst.setTotalQuantityAndPurchases();
    }
    
    
   
}
