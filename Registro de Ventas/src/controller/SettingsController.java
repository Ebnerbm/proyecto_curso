/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import misc.Constants;
import misc.PropertyFile;
import view.DialogSettings;

public class SettingsController implements ActionListener{  
    private final DialogSettings ds;
    private final String directory;

    
    public SettingsController(DialogSettings settingsDialog) {
        this.ds = settingsDialog;
        PropertyFile prop = new PropertyFile();
        prop.open();
        if(prop.isFileExists()){
            this.directory = prop.getProperty(Constants.DIRECTORY_PROPERTY);
            this.ds.setSelectedDirectory(this.directory);
            this.ds.setCompanyName(prop.getProperty(Constants.COMPANYNAME_PROPERTY));
        }
        else{
            this.directory = "";
        }
    }
    
        public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "choose":
                chooseDirectory();
                break;
            case "Acept":
                saveSettings();
                break;
        }
    }

    public void chooseDirectory(){
        JFileChooser chooser = new JFileChooser(); 
        if(this.directory.isEmpty()){
            chooser.setCurrentDirectory(new java.io.File("."));
        }
        else{
            chooser.setCurrentDirectory(new java.io.File(this.directory));
        }
        chooser.setDialogTitle("Selecciona una carpeta");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

        chooser.setAcceptAllFileFilterUsed(false);
            
        if (chooser.showOpenDialog(ds) == JFileChooser.APPROVE_OPTION) {
            ds.setSelectedDirectory(chooser.getSelectedFile().getAbsolutePath());
        }
        else {
            if(ds.getSelectedDirectory().isEmpty()){
                ds.setSelectedDirectory("No ha seleccionado una carpeta");
            }
        }     
    }
    /**
     * Guarda los valores configurados en un archivo llamado config.properties que estará ubicado en la ruta de la aplicación.
     */
    public void saveSettings(){
        PropertyFile prop = new PropertyFile();
        prop.setProperty(Constants.DIRECTORY_PROPERTY, ds.getSelectedDirectory());
        prop.setProperty(Constants.COMPANYNAME_PROPERTY, ds.getCompanyName());
        if(prop.saveProperties()){
            ds.dispose();
        }
        else{
            JOptionPane.showMessageDialog(ds,"Ocurrió un error al guardar la configuración", "Configuración", JOptionPane.ERROR_MESSAGE);
        }
    }
    
}
