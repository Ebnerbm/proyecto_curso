/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import view.FrameLogin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import main.Main;
import model.KeyChain;
import view.FirstForm;

public class LoginController implements ActionListener{
    private FrameLogin frameL;
    private FirstForm firstForm;
    private KeyChain keyChain;

    public LoginController(FrameLogin vl) {
        super();
        frameL = vl;
        keyChain = new KeyChain();
        keyChain.init();
    }   
    
    public LoginController(FirstForm firstForm) {
        this.firstForm = firstForm;
        keyChain = new KeyChain();
        keyChain.init();
    } 

    @Override
    public void actionPerformed(ActionEvent e) {
            switch(e.getActionCommand()){
                case "register": 
                      registerFirstUser();
                      break;
                case "cancel":
                      firstForm.dispose();
                      break;
                case "btnLogin":
                    login();
                    break;
                case "CancelLogin":
                    frameL.dispose();
                    break;
            }
    }

    public void registerFirstUser(){
        keyChain.add(this.firstForm.getData());
        if(keyChain.save()){
            JOptionPane.showMessageDialog(firstForm, "Su cuenta ha sido registrada correctamente. Ahora use sus datos para iniciar sesión.", "Registrar", JOptionPane.INFORMATION_MESSAGE);
            this.firstForm.dispose();
            Main.showLoginForm();
        }
        else{
            JOptionPane.showMessageDialog(firstForm, "El usuario no pudo ser registrado.", "Registrar", JOptionPane.ERROR_MESSAGE);
        } 
    }
    
    public void login(){
        if(keyChain.isAuthorized(this.frameL.getData())){
            this.frameL.dispose();
            Main.showMainForm();
        }
        else
        {
            JOptionPane.showMessageDialog(frameL, "Su inicio de sesión ha fallado, por favor ingrese los datos correctamente", "Login", JOptionPane.ERROR_MESSAGE);
            frameL.clear();
        }
    }
    
}
