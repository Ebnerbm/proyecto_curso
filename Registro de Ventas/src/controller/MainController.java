
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.DialogSettings;
import view.MainFrame;


public class MainController implements ActionListener{
    MainFrame mf;
        
    public MainController(MainFrame mf) {
        this.mf = mf;
    } 

    @Override
    public void actionPerformed(ActionEvent evt) {
        switch(evt.getActionCommand()) {
            case "Menu" :  
                mf.mostrarPanel("pnlMenu");
                break;
            case "Ventas" :
                mf.mostrarPanel("pnlVentas");
                break;
            case "configuration":
                showDialogSettings();
                break;
        }
    }
    
    public void showDialogSettings(){
        DialogSettings sd = new DialogSettings(mf, true);
        sd.setLocationRelativeTo(null);
        sd.setVisible(true);
    }
    
    
}
